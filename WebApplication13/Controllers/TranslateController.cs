﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication13.Models.Api;
using WebApplication13.Models.Dto;
using WebApplication13.Models.Translate;
using WebApplication13.Services;

namespace WebApplication13.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TranslateController : ControllerBase
    {
        private readonly TranslateService _service;

        public TranslateController(TranslateService service)
        {
            _service = service;
        }

        /// <summary>
        /// Gets the list of languages.
        /// </summary>
        /// <returns>The list of languages.</returns>
        /// <response code="200">Returns the list of languages.</response>
        /// <response code="400">If the request is bad.</response>
        /// <response code="500">If there is an internal server error.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse<string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiResponse<string>), StatusCodes.Status500InternalServerError)]
        public async Task<ApiResponse<RootResponse<List<LanguageListResult>>>> GetLanguages([FromQuery] LanguageRequestDto query)
        {
            return await _service.GetLanguages(query);
        }


        /// <summary>
        /// Translates the specified text.
        /// </summary>
        /// <param name="body">The translation request.</param>
        /// <returns>The translation response.</returns>
        /// <response code="200">Returns the translation response.</response>
        /// <response code="400">If the request is bad.</response>
        /// <response code="500">If there is an internal server error.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ApiResponse<TranslateResponse>> Translate(TranslateDto body)
        {
            return await _service.Translate(body);
        }
    }
}
