﻿using WebApplication13.Models.Attributes;

namespace WebApplication13.Models.Dto
{
    public class LanguageRequestDto
    {
        [LanguageCode]
        public string Code { get; set; }

        public string Platform { get; set; }
    }
}
