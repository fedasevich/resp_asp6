﻿using System.ComponentModel.DataAnnotations;
using WebApplication13.Models.Attributes;

namespace WebApplication13.Models.Dto
{
    public class TranslateDto
    {
        [LanguageCode]
        public string From { get; set; }
        
        [LanguageCode]
        [DifferentFrom("From", ErrorMessage = "To must be different from From")]
        public string To { get; set; }

        [MinLength(1, ErrorMessage = "Data must not be empty")]
        public string Data { get; set; }
    }
}
