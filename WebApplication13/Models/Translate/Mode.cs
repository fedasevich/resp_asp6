﻿using System.Text.Json.Serialization;

namespace WebApplication13.Models.Translate
{
    public class Mode
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("value")]
        public bool Value { get; set; }
    }

}
