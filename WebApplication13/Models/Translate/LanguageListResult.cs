﻿using System.Text.Json.Serialization;

namespace WebApplication13.Models.Translate
{
    public class LanguageListResult
    {
        [JsonPropertyName("full_code")]
        public string FullCode { get; set; }

        [JsonPropertyName("code_alpha_1")]
        public string CodeAlpha1 { get; set; }

        [JsonPropertyName("englishName")]
        public string EnglishName { get; set; }

        [JsonPropertyName("codeName")]
        public string CodeName { get; set; }

        [JsonPropertyName("flagPath")]
        public string FlagPath { get; set; }

        [JsonPropertyName("testWordForSyntezis")]
        public string TestWordForSyntezis { get; set; }

        [JsonPropertyName("rtl")]
        public string Rtl { get; set; }

        [JsonPropertyName("modes")]
        public List<Mode> Modes { get; set; }
    }
}
