﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace WebApplication13.Models.Attributes
{
    public class LanguageCodeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var code = value as string;
            if (code != null && Regex.IsMatch(code, @"^[a-z]{2}_[A-Z]{2}$"))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Invalid language code format. It should be in the format 'language code_country code' (e.g., 'en_GB').");
        }
    }
}
