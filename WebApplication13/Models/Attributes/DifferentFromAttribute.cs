﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication13.Models.Attributes
{
    public class DifferentFromAttribute : ValidationAttribute
    {
        private readonly string _otherProperty;

        public DifferentFromAttribute(string otherProperty)
        {
            _otherProperty = otherProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var otherValue = validationContext.ObjectType.GetProperty(_otherProperty).GetValue(validationContext.ObjectInstance);

            if (value != null && otherValue != null && value.ToString() == otherValue.ToString())
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }
    }
}
