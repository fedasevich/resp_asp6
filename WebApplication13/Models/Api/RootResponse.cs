﻿using System.Text.Json.Serialization;

namespace WebApplication13.Models.Api
{
    public class RootResponse<T>
    {
        [JsonPropertyName("err")]
        public string Err { get; set; }

        [JsonPropertyName("result")]
        public T Result { get; set; }
    }
}
