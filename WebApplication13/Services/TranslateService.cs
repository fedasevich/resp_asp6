﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Text.Json;
using WebApplication13.Helpers;
using WebApplication13.Models.Api;
using WebApplication13.Models.Dto;
using WebApplication13.Models.Translate;

namespace WebApplication13.Services
{
    public class TranslateService
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;

        public TranslateService(IConfiguration configuration)
        {
            _configuration = configuration;

            _client = new HttpClient { BaseAddress = new Uri(_configuration.GetValue<string>("BaseAddress")) };
            _client.DefaultRequestHeaders.Add("X-RapidAPI-Key", _configuration.GetValue<string>("ApiKey"));
            _client.DefaultRequestHeaders.Add("X-RapidAPI-Host", _configuration.GetValue<string>("ApiHost"));
        }

        public async Task<ApiResponse<RootResponse<List<LanguageListResult>>>> GetLanguages(LanguageRequestDto query)
        {
            try
            {
               var queryString=  QueryStringHelper.ToQueryString(query);
     
                var response = await _client.GetAsync("getLanguages"+ queryString);
                var content = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<RootResponse<List<LanguageListResult>>>(content);

                if (data != null && data.Err != null)
                {
                    throw new HttpRequestException(data.Err, null, HttpStatusCode.InternalServerError);
                }

                return new ApiResponse<RootResponse<List<LanguageListResult>>>
                {
                    Message = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Data = data
                };
            }
            catch (Exception ex)
            {
                return new ApiResponse<RootResponse<List<LanguageListResult>>>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<ApiResponse<TranslateResponse>> Translate(TranslateDto body)
        {
            try
            {
                var response = await _client.PostAsJsonAsync("translate", body);
                var content = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<TranslateResponse>(content);

                if (data != null && data.Err != null)
                {
                    throw new HttpRequestException(data.Err, null, HttpStatusCode.InternalServerError);
                }

                return new ApiResponse<TranslateResponse>
                {
                    Message = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Data = data
                };
            }
            catch (Exception ex)
            {
                return new ApiResponse<TranslateResponse>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
        }
    }
}

