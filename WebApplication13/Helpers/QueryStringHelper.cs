﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Linq;

namespace WebApplication13.Helpers
{
    public static class QueryStringHelper
    {
        public static string ToQueryString(object obj)
        {
            var nvc = new NameValueCollection();

            var properties = from prop in obj.GetType().GetProperties()
                             where prop.GetValue(obj, null) != null
                             select prop;

            foreach (var property in properties)
            {
                nvc.Add(property.Name, property.GetValue(obj, null).ToString());
            }

            return "?" + HttpUtility.ParseQueryString(nvc.ToString());
        }
    }
}
